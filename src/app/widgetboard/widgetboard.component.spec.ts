import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WidgetboardComponent } from './widgetboard.component';

describe('WidgetboardComponent', () => {
  let component: WidgetboardComponent;
  let fixture: ComponentFixture<WidgetboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WidgetboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
