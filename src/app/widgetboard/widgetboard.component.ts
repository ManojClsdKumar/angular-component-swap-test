import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-widgetboard',
  templateUrl: './widgetboard.component.html',
  styleUrls: ['./widgetboard.component.css']
})
export class WidgetboardComponent implements OnInit {

  constructor(private routerNav: Router, private actRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  setup1() {

  }

  setup2() {

  }

}
