import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { WidgetboardComponent } from './widgetboard/widgetboard.component';
import { WidgetAComponent } from './widgetboard/widget-a/widget-a.component';
import { WidgetBComponent } from './widgetboard/widget-b/widget-b.component';
import { Routes, RouterModule } from '@angular/router';



const appRoutes: Routes = [
  { path: 'a1', component: WidgetAComponent, outlet: 'position1' },
  { path: 'a2', component: WidgetAComponent, outlet: 'position2' },
  { path: 'b1', component: WidgetBComponent, outlet: 'position1' },
  { path: 'b2', component: WidgetBComponent, outlet: 'position2' }
];

@NgModule({
  declarations: [
    AppComponent,
    WidgetboardComponent,
    WidgetAComponent,
    WidgetBComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
