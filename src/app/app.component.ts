import { Router, ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private router: Router, private actRoute: ActivatedRoute) {

  }
  setup1() {

    this.router.navigate([{ outlets: { position2: 'b2', position1: 'a1' } }], {
      relativeTo: this.actRoute
    });


  }

  setup2() {
    this.router.navigate([{ outlets: { position2: 'a2', position1: 'b1' } }], {
      relativeTo: this.actRoute
    });

  }
}

